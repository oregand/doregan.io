---
title: "LLMs"
description: "Lets break down the main difference in a variety of LLMs that are currently in play"
date: "2023-09-13"
---

LLMs, or Language Model Learners, are a type of AI model that are trained to understand and generate human language. 

{{< comment author="oregand" >}}
**overview:** LLMs work by taking a sequence of words (input) and predicting the next word in the sequence (output). 

**detail:** They do this by assigning probabilities to each possible next word and then selecting the word with the highest probability. The input to an LLM is typically a sequence of tokens, where a token can be as short as a single character or as long as a word. The output is also a sequence of tokens, but it's the sequence that the model predicts should come next based on the input.

**summary:** In summary, LLMs are a powerful tool for understanding and generating human language, and they work by predicting the next word in a sequence based on the words that have come before.
{{< /comment >}}
