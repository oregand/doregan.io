---
title: "doregan.io"
description: "A small site managaed by David O'Regan for learning in AI, Cloud Development and Engineering Management learning"
---

## Welcome travler! 

Welcome to doregan.io! This is a small site managed by David O'Regan, dedicated to the exploration and learning of AI, Cloud Development, and Engineering Management. Here, you'll find resources, tutorials, and insights to help you navigate these complex fields. Whether you're a seasoned professional or just starting out, there's something here for everyone. Enjoy your stay!


## Want to help make this better?

Check out the [GitLab project](https://gitlab.com/oregand/doregan.gitlab.io) for this site.
Issues and Merge Requests are welcome!
