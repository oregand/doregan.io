# doregan.io

This is the static site for [doregan.io][1].

## How do I run this thing?

### Prerequisites

- You will need to install [Hugo][2] locally. **Important:** install the "extended" version if relevant to your OS.
- You will need to install [Yarn][3].

### Start local server

```
yarn
yarn start
```

[1]: https://doregan.io/
[2]: https://gohugo.io/getting-started/installing/
[3]: https://classic.yarnpkg.com/en/
